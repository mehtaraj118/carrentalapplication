package com.example.carrental.Controller;

import com.example.carrental.Model.CarDetail;
import com.example.carrental.Service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/carRentals")
public class CarController {

    @Autowired
    private CarService cars;
    @PostMapping("/addNewCar")
    public String addNewCar(@RequestBody CarDetail cardetails){
       return cars.addNewCar(cardetails);
    }

    @GetMapping("/getAllCarsDetails")
    public List<CarDetail> getAllCarsDetails(){
        return cars.getAllCarsDetails();
    }

    @GetMapping("/getCarDetailById/{value}")
    public CarDetail getCarDetailById(@PathVariable("value") int id){

        return cars.getCarDetailById(id);
    }

    @GetMapping("/getCarDetailByName/{value}")
    public CarDetail getCarDetailByName(@PathVariable("value") String carname){
        return cars.getCarDetailByName(carname);
    }

    @PutMapping("/updateCarDetail")
    public CarDetail updateCarDetails(@RequestBody CarDetail cardetail)
    {
        return cars.updateCarDetails(cardetail);
    }

    @GetMapping("/getCarDetailByIdAndStatus/{value}")
    public String getCarDetailByIdAndStatus(@PathVariable("value") int id) throws Exception{
        String result="";
        CarDetail detail= cars.getCarDetailById(id);
        String status= detail.getStatus();
        System.out.println(status.length());
        System.out.println(status.equalsIgnoreCase("Available"));
        if(status.equalsIgnoreCase("Available"))
            result= "Success";
        else
             throw new Exception("Not Available");
        return result;
    }
}
