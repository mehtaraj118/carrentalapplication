package com.example.carrental.Repository;

import com.example.carrental.Model.BookingDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<BookingDetails,Integer> {
}
