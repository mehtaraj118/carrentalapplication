package com.example.carrental.Service;

import com.example.carrental.Model.CarDetail;
import com.example.carrental.Repository.CarRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {
    @Autowired
    private CarRepository repo;

        public String addNewCar(CarDetail cardetail){
            repo.save(cardetail);
            return "New Car Details added Successfully!!";
        }

        public List<CarDetail> getAllCarsDetails(){

            return repo.findAll();
        }

        public CarDetail getCarDetailById(int id){

            return repo.findById(id).orElse(null);
        }

        public CarDetail getCarDetailByName(String name){

            return repo.findByCarname(name);
        }

        public CarDetail updateCarDetails(CarDetail cardetail) {
            CarDetail existingcar = repo.findById(cardetail.getId()).orElse(null);
            if (cardetail.getCarname() != null) {
                existingcar.setCarname(cardetail.getCarname());
            } else {
                existingcar.setCarname(existingcar.getCarname());
            }
            if (cardetail.getStatus() != null) {
                existingcar.setStatus(cardetail.getStatus());
            } else {
                existingcar.setStatus(existingcar.getStatus());
            }
            if (cardetail.getPrice() != 0.0) {
                existingcar.setPrice(cardetail.getPrice());
            } else {
                existingcar.setPrice(existingcar.getPrice());
            }
            if (cardetail.getUrl() != null) {
                existingcar.setUrl(cardetail.getUrl());
            } else {
                existingcar.setUrl(existingcar.getUrl());
            }
            if (cardetail.getReg_no() != null) {
                existingcar.setReg_no(cardetail.getReg_no());
            } else {
                existingcar.setReg_no(existingcar.getReg_no());
            }
            if (cardetail.getInsurance_end() != null) {
                existingcar.setInsurance_end(cardetail.getInsurance_end());
            } else {
                existingcar.setInsurance_end(existingcar.getInsurance_end());
            }
            if (cardetail.getP_dtype() != null) {
                existingcar.setP_dtype(cardetail.getP_dtype());
            } else {
                existingcar.setP_dtype(existingcar.getP_dtype());
            }
            if (cardetail.getA_mtype() != null) {
                existingcar.setA_mtype(cardetail.getA_mtype());
            } else {
                existingcar.setA_mtype(existingcar.getA_mtype());
            }
            if (cardetail.getSeater() != 0) {
                existingcar.setSeater(cardetail.getSeater());
            } else {
                existingcar.setSeater(existingcar.getSeater());
            }
            if (cardetail.getAc_nonac() != null) {
                existingcar.setAc_nonac(cardetail.getAc_nonac());
            } else {
                existingcar.setAc_nonac(existingcar.getAc_nonac());
            }
                return repo.save(existingcar);


        }
}
