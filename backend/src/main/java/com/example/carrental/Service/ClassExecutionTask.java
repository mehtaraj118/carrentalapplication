package com.example.carrental.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
@Component
public class ClassExecutionTask {
    @Autowired
    private JavaMailSender javaMailSender;

    long delayfornextstart = 7 * 1000; // delay in ms : 10 * 1000 ms = 10 sec.
    LoopTask tasktoexecute = new LoopTask();
    Timer timer = new Timer("TaskName");

    public void start() throws ParseException, InterruptedException {
        timer.cancel();
        timer = new Timer("TaskName");
//@SuppressWarnings("deprecation")
//Date executionDate = new Date(2013-05-04); // no params = now
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("waiting for the rght day to come");
        Date executionDate = sdf.parse("2021-05-11");

        Date date1 = sdf.parse("2021-05-11");

        Date date2 = sdf.parse("2021-05-11");
        System.out.println(date1 + "and" + date2);
        long waitTill = getTimeDiff(date2, date1);
        if (date2 == date1) {
            waitTill = 0;
            System.out.println(waitTill);
        }

        System.out.println(waitTill);
        Thread.sleep(waitTill);
        timer.scheduleAtFixedRate(tasktoexecute, executionDate, delayfornextstart);
    }

    private class LoopTask extends TimerTask {
        @Override
        public void run() {
            System.out.println("sending mail");
            SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
            simpleMailMessage.setFrom("carrentalservicesind@gmail.com");
            simpleMailMessage.setTo("anujmundra5@gmail.com");
            simpleMailMessage.setSubject("coupon");
            simpleMailMessage.setText("Hii Anuj");
            javaMailSender.send(simpleMailMessage);
            System.out.println("mail successfully sent");
        }
    }

    public static void main(String[] args) throws ParseException, InterruptedException {
        ClassExecutionTask executingTask = new ClassExecutionTask();
        executingTask.start();
    }

    public static long getTimeDiff(Date dateOne, Date dateTwo) {
        String diff = "";
        long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());
        diff = String.format("%d hour(s) %d min(s)", TimeUnit.MILLISECONDS.toHours(timeDiff),
                TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));
        return timeDiff;
    }
}

