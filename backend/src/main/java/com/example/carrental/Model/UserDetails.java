package com.example.carrental.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class UserDetails {
    @Id
    private String id;
    private String name;
    private String password;
    private String contact;
    private String address;
    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<BookingDetails> bookings = new HashSet<>();



}
