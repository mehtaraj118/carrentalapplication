import React from 'react';
import LoginForm from './Components/LoginForm';
import Navbar from './Components/navbar';
import RegisterForm from './Components/RegisterForm';
import Home from './Components/Home';
import Cars from './Components/Cars';
import { 
  BrowserRouter as Router, 
  Route,  
  Switch 
} from 'react-router-dom'; 
import Booking from './Components/Booking';
import Confirmation from './Components/Confirmation';
import CarNotAvailable from './Components/CarNotAvailable';
import InvalidUser from './Components/InvalidUser';
import AdminUser from './Components/AdminUser';
import AdminCar from './Components/AdminCar';
import AdminBooking from './Components/AdminBooking';
import AdminAddCar from './Components/AdminAddCar';
import AdminHome from './Components/AdminHome';
import Coupon from './Components/Coupon';
import AdminLogin from './Components/AdminLogin';
import AboutUs from './Components/AboutUs';
function App() {
  return (
    <Router>
    <div className="App">
    <Switch> 
    <Route exact path='/' component={Home}></Route> 
    <Route exact path='/LoginForm' component={LoginForm}></Route> 
    <Route exact path='/RegisterForm' component={RegisterForm}></Route> 
    <Route exact path='/Cars' component={Cars}></Route> 
    <Route exact path='/Booking' component={Booking}></Route>
    <Route exact path='/Confirmation' component={Confirmation}></Route>
    <Route exact path='/CarNotAvailable' component={CarNotAvailable}></Route>
    <Route exact path='/InvalidUser' component={InvalidUser}></Route>
    <Route exact path='/AdminUser' component={AdminUser}></Route>
    <Route exact path='/AdminCar' component={AdminCar}></Route>
    <Route exact path='/AdminBooking' component={AdminBooking}></Route>
    <Route exact path='/AdminAddCar' component={AdminAddCar}></Route>
    <Route exact path='/AdminHome' component={AdminHome}></Route>
    <Route exact path='/Coupon' component={Coupon}></Route>
    <Route exact path='/AdminLogin' component={AdminLogin}></Route>
    <Route exact path='/AboutUs' component={AboutUs}></Route>
  </Switch> 
    </div>
    </Router>
    
  );

}

export default App;
