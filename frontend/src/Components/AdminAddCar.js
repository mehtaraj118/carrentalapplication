import axios from 'axios';
import React, { Component } from 'react'
import carrentallogo from '../Components/carrentallogo.jpg';
import Navbaradmin from './navbaradmin';
class AdminAddCar extends Component {
    constructor(props) {
            super(props);
            this.state = {
              id: '',
              carname: '',
              status: '',
              price: '',
              url: '',
              reg_no:'',
              insurance_end:'',
              p_dtype:'',
              a_mtype:'',
              seater:'',
              ac_nonac:''
            }
            this.onChange=this.onChange.bind(this);
            this.postData=this.postData.bind(this);
        }
        onChange(e){
            this.setState({
                [e.target.name]:e.target.value
            });
        }
        postData(e){
            e.preventDefault();
            axios.post("http://localhost:8080/carRentals/addNewCar",this.state)
            .then((response)=>{
                if(response.status==200)
                {
                    <script>
                    {alert("Car Successfully Added !!!")}
                    </script>
                }
            })
            .catch((error)=>{
                {alert("Error Car Cannot Be Added !!!")}
            
        })
            
        }
    render(){
    return (
        <div className="addcar" style={{backgroundColor:'whitesmoke'}}>
        <Navbaradmin/>
        <form onSubmit={this.postData}>
            <div className="form-inner">
                <h2>Add New Car</h2>
                <div className="form-group">
                    <img src={carrentallogo} width="100px" height="100px" alt="carimage"/>
                </div>
                <div className="form-group">
                    <label htmlFor="id">Id :</label>
                    <input type="number" name="id" id="id" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="carname">Car Name :</label>
                    <input type="text" name="carname" id="carname" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="status">Availability Status :</label>
                    <input type="text" name="status" id="status" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="price">Price Per KM :</label>
                    <input type="number" name="price" id="price" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="url">Image URL :</label>
                    <input type="text" name="url" id="url" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="reg_no">Registration Number :</label>
                    <input type="text" name="reg_no" id="reg_no" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="insurance_end">Insurance End Date :</label>
                    <input type="text" name="insurance_end" id="insurance_end" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="p_dtype">Petrol/Diesel Type :</label>
                    <input type="text" name="p_dtype" id="p_dtype" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="a_mtype">Automatic/Manual Type :</label>
                    <input type="text" name="a_mtype" id="a_mtype" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="seater">Seater :</label>
                    <input type="number" name="seater" id="seater" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="ac_nonac">AC/Non-AC Type:</label>
                    <input type="text" name="ac_nonac" id="ac_nonac" onChange={this.onChange}/>
                </div>
                <input type="submit" value="Add Car"/>
            </div>
        </form>
        </div>

    )
    }
}

export default AdminAddCar;
