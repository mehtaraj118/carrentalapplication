import React, {useState} from 'react';
import {   
    Link 
  } from 'react-router-dom';
import carrentalbgimg from '../Components/carrentalbgimg2.jpeg';
import imageshai from '../Components/backgroundimage4.png';
import { Button, Modal } from 'react-bootstrap';
import Navbar1 from './navbar1' 
function App() {
  const [show, setShow] = useState(true);
   
  const handleClose = () => setShow(false);
 
   
  return (
      <div className="home1">
      <Navbar1/>
      <div className="home" style={{marginTop:'-150px'}}>
      <div className="bgimg">
      <img src={imageshai} height="1000px" width="100%" alt="carimage"/>
      </div>

      <Modal size="lg" show={show} >
        <Modal.Header >
          <Modal.Title><h2 style={{color: "green"}}>Congratulations !!! Your Booking is Confirmed</h2></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h6>Please Visit To The Center With Following Documents For Verification On The Travel Date</h6>
          <p style={{color: "red"}}>1) Driving License</p>
          <p style={{color: "red"}}>2) Aadhar Card</p>
          <h6>We Wish You A Happy And Prosperous Journey !!!</h6>
          <div className="navbar">
          <div className="button4" style={{marginLeft:"300px"}}>
            <button style={{alignItems:"center"}}><Link style={{ textDecoration: 'none' ,color:'black'}}  to="/LoginForm">LogOut</Link></button>
            </div>
            </div>
            <div className="navbar">
          <div className="button4" style={{marginLeft:"257px"}}>
            <button style={{alignItems:"center"}}><Link style={{ textDecoration: 'none' ,color:'black'}}  to="/Cars">Do More Bookings</Link></button>
            </div>
            </div>
        </Modal.Body>
      </Modal>
    </div>
    </div>
  );
}
   
export default App;