import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react'
import Navbaradmin from './navbaradmin';
function AdminCar() {
  const [cars, setCar] = useState([])
  const [carname, setCarName] = useState("");
  const [status, setStatus] = useState("");
  const [price, setPrice] = useState();
  const [url, setUrl] = useState("");
  const [reg_no, setReg] = useState("");
  const [insurance_end, setInsuranceEnd] = useState("");
  const [p_dtype, setP_DType] = useState("");
  const [a_mtype, setA_MType] = useState("");
  const [seater, setSeater] = useState();
  const [ac_nonac, setAc_NonacType] = useState("");
  const [id,setId]=useState();

  useEffect(() => {
    getCars();
  }, [])
  function getCars() {
    fetch("http://localhost:8080/carRentals/getAllCarsDetails").then((result) => {
      result.json().then((resp) => {
        // console.warn(resp)
        console.log(resp);
        setCar(resp)
        setCarName(resp[0].carname)
        setStatus(resp[0].status)
        setPrice(resp[0].price)
        setUrl(resp[0].url)
        setReg(resp[0].reg_no)
        setInsuranceEnd(resp[0].insurance_end)
        setP_DType(resp[0].p_dtype)
        setA_MType(resp[0].a_mtype)
        setSeater(resp[0].seater)
        setAc_NonacType(resp[0].ac_nonac)
        setId(resp[0].id)
      })
    })
  }

  async function selectCar(id)
  {
    let item=cars;
    //console.log(item[0].name);
    for(let i=0;i<item.length;i++)
    {
        
        if(item[i].id==id)
        {
            setCarName(item[i].carname)
            setStatus(item[i].status)
            setPrice(item[i].price)
            setUrl(item[i].url)
            setReg(item[i].reg_no)
            setInsuranceEnd(item[i].insurance_end)
            setP_DType(item[i].p_dtype)
            setA_MType(item[i].a_mtype)
            setSeater(item[i].seater)
            setAc_NonacType(item[i].ac_nonac)
            setId(item[i].id)
        }
    }
      /*  setName(item.name)
        setAddress(item.email)
        setMobile(item.mobile);
        setUserId(item.id)*/
  }
async function updateCar()
  {
    let item={id,carname,status,price,url,reg_no,insurance_end,p_dtype,a_mtype,seater,ac_nonac}
    console.warn("item",item)
    fetch(`http://localhost:8080/carRentals/updateCarDetail`, {
      method: 'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json'
      },
      body:JSON.stringify(item)
    }).then((result) => {
      result.json().then((resp) => {
        console.warn(resp)
        getCars()
      })
    })
  }
  return (
    <div style={{backgroundColor:'whitesmoke'}}>
      <Navbaradmin/>
      <h1 style={{textAlign:'center',color:'darkcyan',fontFamily:'cursive'}}>Cars Data</h1>
        
      <Table striped bordered hover variant="dark" style={{marginTop:'10px'}}>
        <tbody>
          <tr>
            <td style={{fontWeight:'bolder'}}>ID</td>
            <td style={{fontWeight:'bolder'}}>Name</td>
            <td style={{fontWeight:'bolder'}}>Status</td>
            <td style={{fontWeight:'bolder'}}>Price per km</td>
            <td style={{fontWeight:'bolder'}}>Image</td>
            <td style={{fontWeight:'bolder'}}>Registration No</td>
            <td style={{fontWeight:'bolder'}}>Insurance End</td>
            <td style={{fontWeight:'bolder'}}>P/DType</td>
            <td style={{fontWeight:'bolder'}}> A/MType</td>
            <td style={{fontWeight:'bolder'}}>Seater</td>
            <td style={{fontWeight:'bolder'}}>AC/Nonac</td>
            <td style={{fontWeight:'bolder'}}>Operations</td>
          </tr>
          {
            cars.map((item, i) =>
              <tr key={i}>
                <td>{item.id}</td>
                <td>{item.carname}</td>
                <td>{item.status}</td>
                <td>{item.price}</td>
                <td><img src={item.url} alt="carimage" style={{width:'150px',height:'100px'}}/></td>
                <td>{item.reg_no}</td>
                <td>{item.insurance_end}</td>
                <td>{item.p_dtype}</td>
                <td>{item.a_mtype}</td>
                <td>{item.seater}</td>
                <td>{item.ac_nonac}</td>
                <td><button style={{backgroundColor:'Cyan',borderRadius:'5px'}} onClick={() => selectCar(item.id)}>Update</button></td>
              </tr>
            )
          }
        </tbody>
      </Table>
     
      <h2 style={{textAlign:'center',color:'darkcyan',fontFamily:'cursive'}}>Edit Here</h2>
      <form>
      <div className="form-inner">
      <div className="form-group">
      <label> Name:</label>
      <input type="text" value={carname} onChange={(e)=>{setCarName(e.target.value)}} autoFocus/>
      </div>
      <div className="form-group">
      <label> Status:</label><br/>
      <input type="text" value={status} onChange={(e)=>{setStatus(e.target.value)}} />
      </div>
      <div className="form-group">
      <label> Price:</label><br/>
      <input type="text"  value={price}  onChange={(e)=>{setPrice(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> Image URL:</label><br/>
      <input type="text"  value={url}  onChange={(e)=>{setUrl(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> Registration Number:</label><br/>
      <input type="text"  value={reg_no}  onChange={(e)=>{setReg(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> Insurance End Date:</label><br/>
      <input type="text"  value={insurance_end}  onChange={(e)=>{setInsuranceEnd(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> Petrol/Diesel:</label><br/>
      <input type="text"  value={p_dtype}  onChange={(e)=>{setP_DType(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> Automatic/Manual:</label><br/>
      <input type="text"  value={a_mtype}  onChange={(e)=>{setA_MType(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> Seater:</label><br/>
      <input type="text"  value={seater}  onChange={(e)=>{setSeater(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <label> AC/Non-AC:</label><br/>
      <input type="text"  value={ac_nonac}  onChange={(e)=>{setAc_NonacType(e.target.value)}}/> 
      </div>
      <div className="form-group">
      <input type="submit" value="Update Car" onClick={updateCar}/>
      </div>
      </div>
      </form>
    </div>
  );
}
export default AdminCar;
