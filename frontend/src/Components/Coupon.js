import React, { Component } from 'react'

import Navbaradmin from './navbaradmin';
class Coupon extends Component {
    constructor(props) {
        super(props);
        this.state = {
                message:"",
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        });
    }
     async postData(UserData){
        <script>
        {alert("Message Sent")}
        </script>
        return new Promise(()=>{
            fetch('http://localhost:8080/getID',{
                method:'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify(UserData)
            })
            .then(response => {
                
                if(response.status==200)
                {
                    console.log("Messages Sent Successfully")
                    
                }
                else{
                    this.props.history.push("/LoginForm");
                    <script>
                    {alert("Invalid Credentials!!")}
                    </script>
                }
                return response.json();
            })
        })
    }
    handlerLogin = (e) => {  
                e.preventDefault(); 
                this.postData(this.state);
              //  this.props.history.push("/Cars");
        }
    render(){
    return (
        <div className="coupon">
       
        <form method="POST" style={{marginTop:'200px',boxShadow:'10px 15px 15px 10px'}}>
            <div className="form-inner">
          
            <div class="form-group shadow-textarea">
            <label for="exampleFormControlTextarea6" style={{marginLeft:'130px',marginBottom:'20px',fontWeight:"bold"}}>Coupon</label>
            <textarea class="form-control z-depth-1" id="exampleFormControlTextarea6" rows="3" placeholder="Enter Coupon Details Here..."  onChange={(e) => this.handleChange(e)}></textarea>
            </div>
            <input type="submit" value="Send Coupon" onClick={this.handlerLogin}/>
            </div>
        </form>
        </div>
    )
    }
}

export default Coupon;
