import React from 'react';
import carrentallogo from '../Components/carrentallogo.jpg';
import {   
    Link 
  } from 'react-router-dom';
function navbaradmin() {
    return (
        <div className="navbar">
            <div className="logo" style={{width:'1100px'}}>
            <img src={carrentallogo} width="50px" height="50px" alt="carimage"/>
            <h1 style={{marginLeft:'415px',marginTop:'-50px',fontWeight:'bolder',color:'#c2d6d6',fontFamily:'serif',textShadow:'2px 2px tomato'}}>CAR RENTAL SERVICES INDIA</h1>
            </div>
            <div className="button3">
            <button><Link style={{ textDecoration: 'none' ,color:'black'}} to="/AdminHome">Home</Link></button>
            </div>
            <div className="button3">
            <button><Link style={{ textDecoration: 'none' ,color:'black'}} to="/AdminLogin">Logout</Link></button>
            </div>
        </div>
    )
}

export default navbaradmin;
