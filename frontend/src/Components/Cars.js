import React  from 'react';
import {Card} from 'react-bootstrap';
import {ListGroup,ListGroupItem,Button} from 'react-bootstrap';
import { Component } from 'react';
import Navbar1 from './navbar1'
import navbar1 from './navbar1';
class Cars extends Component{

    constructor(props)
    {
        super(props);
        this.state={
            datas:[],
            isLoaded:false,
        }
    }
    book = () => {   
        this.props.history.push("/Booking");
}
    componentDidMount(){
        fetch('http://localhost:8080/carRentals/getAllCarsDetails')
        .then(res =>res.json()
        .then(json =>{
            this.setState(
                {
                    isLoaded:true,
                    datas:json
                }
            )
            console.log(res.status);
        }))
    }
    render(){
        var {isLoaded,datas}=this.state;
        if(!isLoaded){
            return <div style={{textAlign:'center',marginTop:'300px'}}><h1><b>Loading.....</b></h1></div>
        }
        else {
        return(
            <div className="car">
            <Navbar1/>
            <div className="Cars" >
            {datas.map(data =>(
                <Card style={{ width: '20rem' ,marginInline:'15px',marginBottom:'65px',borderStyle:'solid',marginTop:'-10px',marginLeft:'22px'}}>
                <Card.Img variant="top" src={data.url} width='150px' height='200px'/>
                <Card.Body style={{backgroundImage:'linear-gradient(to right bottom,#f8f4f5 ,#7cd9f0)'}}>
                  <Card.Title><h3>ID - {data.id}</h3></Card.Title>
                  <Card.Text>
                   <h3><b>{data.carname}</b></h3>
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush" >
                  <ListGroupItem ><h5><b>Price per KM:</b><br></br>{data.price}</h5></ListGroupItem>
                  <ListGroupItem ><h5><b>Status of Availability:</b><br></br>{data.status}</h5></ListGroupItem>
                  <ListGroupItem ><h5><b>Petrol/Diesel:</b><br></br>{data.p_dtype}</h5></ListGroupItem>
                  <ListGroupItem ><h5><b>Automatic/Manual:</b><br></br>{data.a_mtype}</h5></ListGroupItem>
                  <ListGroupItem ><h5><b>Seater:</b><br></br>{data.seater}</h5></ListGroupItem>
                  <ListGroupItem ><h5><b>AC/Non-AC:</b><br></br>{data.ac_nonac}</h5></ListGroupItem>
                </ListGroup>
                <Button variant="primary" style={{marginTop:'5px',marginBottom:'5px'}} onClick={this.book} disabled={data.status=="Unavailable"}>Book Now</Button>
              </Card>))}
            </div>
            </div>
        );
        }
    }
}
export default Cars;
