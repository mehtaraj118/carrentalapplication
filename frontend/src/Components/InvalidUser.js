import React, {useState} from 'react';
import {   
    Link 
  } from 'react-router-dom';
import carrentalbgimg from '../Components/carrentalbgimg2.jpeg';
import imageshai from '../Components/backgroundimage4.png';
import { Button, Modal } from 'react-bootstrap';
import Navbar1 from './navbar1'
function InvalidUser() {
  const [show, setShow] = useState(true);
   
  const handleClose = () => setShow(false);
 
   
  return (
      <div className="home1">
      <Navbar1/>
      <div className="home" style={{marginTop:'-150px'}}>
      <div className="bgimg">
      <img src={imageshai} height="1000px" width="100%" alt="carimage"/>
      </div>

      <Modal size="lg" show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title><h2 style={{color: "red"}}>OOPS !!! Looks Like The Email Is Not Registered</h2></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h2 style={{marginLeft:"250px",color:"steelblue"}}>Try Booking Again</h2>
          <div className="navbar">
          <div className="button4" style={{marginLeft:"300px"}}>
            <button style={{alignItems:"center"}}><Link style={{ textDecoration: 'none' ,color:'black'}}  to="/Booking">Booking</Link></button>
            </div>
            </div>
        </Modal.Body>
      </Modal>
    </div>
    </div>
  );
}
   
export default InvalidUser;