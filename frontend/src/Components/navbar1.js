import React from 'react';
import carrentallogo from '../Components/carrentallogo.jpg';
import {   
    Link 
  } from 'react-router-dom';
function navbar1() {
    return (
        <div className="navbar">
            <div className="logo">
            <img src={carrentallogo} width="50px" height="50px" alt="carimage"/>
            </div>
            <h1 style={{marginLeft:'50px',fontWeight:'bolder',color:'#c2d6d6',fontFamily:'serif',textShadow:'2px 2px tomato'}}>CAR RENTAL SERVICES INDIA</h1>
            
            <div className="button4">
            <button><Link style={{ textDecoration: 'none' ,color:'black'}}  to="/LoginForm">Logout</Link></button>
            </div>
        </div>
    )
}

export default navbar1;
