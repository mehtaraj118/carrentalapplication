import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import carrentallogo from '../Components/carrentallogo.jpg';
import Navbar from './navbar';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { useEffect, useState,Component } from 'react';
import { Link } from 'react-router-dom';
import Navbaradmin from './navbaradmin';
import gifticon from '../Components/gifticon.png';
import Popup from 'reactjs-popup';
import Coupon from './Coupon';
/*const useStyles = makeStyles((theme) => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  heroContent: {
    padding: theme.spacing(5, 0, 1),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(8),
  },
  
}));
*/
const tiers = [
  {
   
    price: 'Registered Users',
    description: [],
    buttonText: 'Click To See All Users',
    buttonVariant: 'contained',
  },
  {
    
    price: 'All Cars',
    description: [
    ],
    buttonText: 'Click To See All Cars',
    buttonVariant: 'contained',
  },
  {
    
    price: 'Add Car',
    description: [
      
    ],
    buttonText: 'Click To Add A New Car',
    buttonVariant: 'contained',
  },
  {
    price: 'All Bookings',
    description: [
    ],
    buttonText: 'Click To See All Bookings',
    buttonVariant: 'contained',
  },
];

 
class AdminHome extends Component {
 
   //classes = useStyles();
   toggle(value){
    
    if(value=="Registered Users")
    {
      
      this.props.history.push("/AdminUser");
    }
    else if(value=="All Cars")
    {
      this.props.history.push("/AdminCar");
      
    }
    else if(value=="Add Car")
    {
      this.props.history.push("/AdminAddCar");
     
    }
    else if(value=="All Bookings")
    {
      this.props.history.push("/AdminBooking");
    
    }
    
  }
  render()
  {
  return (
    <div className="admin" >
    <Navbaradmin/>
    <React.Fragment>
      <CssBaseline />
      <AppBar position="static" color="default" elevation={0}  style={{ borderBottom: `1px solid `}}/*className={classes.appBar}*/>
        
      </AppBar>
      <Container maxWidth="sm" component="main"  style={{padding:'35px 12px 10px'}}/*className={classes.heroContent}*/>
        
       
      </Container>
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {tiers.map((tier) => (
            // Enterprise card is full width at sm breakpoint
            <Grid item key={tier.title} xs={12}  md={6} >
              <Card style={{backgroundColor:'#33414f',boxShadow:'5px 10px 15px 10px'}}>
                <CardHeader
                  title={tier.title}
                  subheader={tier.subheader}
                  titleTypographyProps={{ align: 'center' }}
                  subheaderTypographyProps={{ align: 'center' }}
                  /*className={classes.cardHeader}*/
                />
                <CardContent style={{marginTop:'40px'}}>
                  <div /*className={classes.cardPricing}*/ >
                    <Typography component="h2" variant="h3" color="textPrimary" style={{color:'whitesmoke',textAlign:'center',fontWeight:'bold',fontFamily:'serif'}}>
                      {tier.price}
                    </Typography>
                  </div>
                  <ul>
                    {tier.description.map((line) => (
                      <Typography component="li" variant="subtitle1" align="center" key={line}>
                        {line}
                      </Typography>
                    ))}
                  </ul>
                </CardContent>
                <CardActions>
                  <Button fullWidth variant={tier.buttonVariant} color="primary"  style={{fontWeight:'bold',backgroundColor:'#c3d7f7',color:'black'}} onClick={(event)=>this.toggle(tier.price)}>
                  
                    {tier.buttonText}
                  </Button>
                </CardActions>
              </Card>
              
            </Grid>
          ))}
        </Grid>
        <div className="icon" style={{marginTop:'50px' ,marginLeft:'410px'}}>
                      
        <Popup trigger={<button> <img src={gifticon} width="100px" height="100px" alt="gifticon"></img></button>} position="top center">
    <div>
    <Coupon/>
    </div>
  </Popup>
              </div>
      </Container>
    </React.Fragment>
    </div>
  );
}
}
 export default AdminHome;