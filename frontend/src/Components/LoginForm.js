import React, { Component } from 'react'
import carrentallogo from '../Components/carrentallogo.jpg';
import Navbar from './navbar';
class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
                id:"",
                password: ""
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        });
    }
     postData(UserData){
         
        return new Promise(()=>{
            fetch('http://localhost:8080/carRentals/login',{
                method:'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify(UserData)
            })
            .then(response => {
                if(response.status==200)
                {
                    this.props.history.push("/Cars");
                }
                else{
                    this.props.history.push("/LoginForm");
                    <script>
                    {alert("Invalid Credentials!!")}
                    </script>
                }
                return response.json();
            })
        })
    }
    handlerLogin = (e) => {  
                e.preventDefault(); 
                this.postData(this.state);
              //  this.props.history.push("/Cars");
        }
    render(){
    return (
        <div className="login">
        <Navbar/>
        <form method="POST">
            <div className="form-inner">
          
                <h2>Login</h2>
                <div className="form-group">
                    <img src={carrentallogo} width="100px" height="100px" alt="carimage"/>
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email :</label>
                    <input type="email" name="id" id="id" onChange={(e) => this.handleChange(e)} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password :</label>
                    <input type="password" name="password" id="password" onChange={(e) => this.handleChange(e)} />
                </div>
                <input type="submit" value="Login" onClick={this.handlerLogin}/>
            </div>
        </form>
        </div>
    )
    }
}

export default LoginForm;
