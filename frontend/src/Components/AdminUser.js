import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react'
import Navbaradmin from './navbaradmin';
function AdminUser() {
  const [users, setUser] = useState([])

  useEffect(() => {
    getUsers();
  }, [])
  function getUsers() {
    fetch("http://localhost:8080/carRentals/getAllUserDetails").then((result) => {
      result.json().then((resp) => {
        // console.warn(resp)
        setUser(resp)
      })
    })
  }
  return (
    <div style={{backgroundColor:'whitesmoke'}}>
      <Navbaradmin/>
      <h1 style={{textAlign:'center',color:'darkcyan',fontFamily:'cursive'}}>User Data</h1>
      <Table striped bordered hover variant="dark" style={{marginTop:'10px'}}>
        <tbody>
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Address</td>
            <td>Mobile</td>
          </tr>
          {
            users.map((item, i) =>
              <tr key={i}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.address}</td>
                <td>{item.contact}</td>
              </tr>
            )
          }
        </tbody>
      </Table>
     
      
    </div>
  );
}
export default AdminUser;
