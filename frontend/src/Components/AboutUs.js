import React, { Component } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBCard,  MDBCardBody } from 'mdbreact';
import imageshai from '../Components/backgroundimage4.png';
import imageshai1 from '../Components/booknow.jpeg';
import Navbar from './navbar';
class AboutUs extends Component {
  render() {
    return (
        <div className="aboutus">
        <Navbar/>
      
      <MDBContainer>
      
        <MDBRow>

          <MDBCol className="mb-4 px-2" >
            <MDBCard className="indigo text-center z-depth-2" style={{backgroundColor:'whitesmoke'}}>
              <MDBCardBody>
              <h1 style={{fontFamily:'serif',fontWeight:'bold',color:'tomato',textShadow:'2px 2px burlywood'}}>About Us</h1>
              <img src={imageshai} height="500px" width="1000px" alt="carimage"/>
              <br/>
            
                <h3 style={{fontFamily:'serif',fontWeight:'bold',color:'seagreen',textShadow:'2px 2px burlywood'}}> Book cab/car on rent online in India at affordable prices.</h3>
                <h3 style={{fontFamily:'serif',fontWeight:'bold',color:'tomato',textShadow:'2px 2px burlywood'}}> Our website provides cheap cab/car hire services in all cities of India.</h3>  
                <img src={imageshai1} height="50px" width="50px" alt="carimage"/>         
                </MDBCardBody>
            </MDBCard>
          </MDBCol>

         
          

          
        </MDBRow>
      </MDBContainer>
      </div>
    );
  };
}

export default AboutUs;