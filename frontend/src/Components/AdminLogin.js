import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Navbaradmin1 from './navbaradmin1';
import { useHistory } from "react-router-dom";
export default function AdminLogin() {
    let history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }
  function checkCreds() {
      if(email=="anujmundra049@gmail.com" && password=="anuj123")
      history.push("/AdminHome");
      else
      {
          <script>
          {alert("Invalid Credentials!!")}
          </script>
      }
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

  return (
    <div className="login">
    <Navbaradmin1/>
      <Form onSubmit={handleSubmit} method="POST" style={{marginTop:'150px'}}>
      
      <div className="form-inner" style={{boxShadow:'5px 10px 15px 10px'}}>
      <div className="admindiv" >
      <h2 style={{color:'tomato',textAlign:'center'}}>Admin Login</h2><br></br>
      </div>
        <Form.Group size="lg" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Button block size="lg" type="submit" disabled={!validateForm()} onClick={() => checkCreds()}>
          Login
        </Button>
        </div>
      </Form>
    </div>
  );
}