import axios from 'axios';
import React, { Component } from 'react'
import carrentallogo from '../Components/carrentallogo.jpg';
import Navbar from './navbar'
class RegisterForm extends Component {
    constructor(props) {
            super(props);
            this.state = {
              name: '',
              address: '',
              contact: '',
              id: '',
              password: ''
            }
            this.onChange=this.onChange.bind(this);
            this.postData=this.postData.bind(this);
        }
        onChange(e){
            this.setState({
                [e.target.name]:e.target.value
            });
        }
        postData(e){
            e.preventDefault();
            axios.post("http://localhost:8080/carRentals/addNewUser",this.state)
            .then((response)=>{
                if(response.status==200)
                {
                    <script>
                    {alert("User Successfully Registered . Kindly Login!!!")}
                    </script>
                }
            })
            .catch((error)=>{
                this.props.history.push("/CarNotAvailable");
                console.log(error);
            
        })
            
        }
    render(){
    return (
        <div className="register">
        <Navbar/>
        <form onSubmit={this.postData}>
            <div className="form-inner">
                <h2>Register</h2>
                <div className="form-group">
                    <img src={carrentallogo} width="100px" height="100px" alt="carimage"/>
                </div>
                <div className="form-group">
                    <label htmlFor="name">Name :</label>
                    <input type="text" name="name" id="name" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="address">Address :</label>
                    <input type="text" name="address" id="address" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="contact">Contact Number :</label>
                    <input type="text" name="contact" id="contact" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="id">Email :</label>
                    <input type="email" name="id" id="id" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password :</label>
                    <input type="password" name="password" id="password" onChange={this.onChange}/>
                </div>
                <input type="submit" value="Register"/>
            </div>
        </form>
        </div>
    )
    }
}

export default RegisterForm;
