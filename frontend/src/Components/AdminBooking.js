import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react';
import Navbaradmin from './navbaradmin';
function AdminBooking() {
  const [booking, setBooking] = useState([])
  

  useEffect(() => {
    getCars();
  }, [])
  function getCars() {
    fetch("http://localhost:8080/getAllBookings").then((result) => {
      result.json().then((resp) => {
        // console.warn(resp)
        console.log(resp);
        setBooking(resp)
       
      })
    })
  }
  return (
    <div style={{backgroundColor:'whitesmoke'}}>
      <Navbaradmin/>
      <h1 style={{textAlign:'center',color:'darkcyan',fontFamily:'cursive'}}>All Booking Details</h1>
        
      <Table striped bordered hover variant="dark" style={{marginTop:'10px'}}>
        <tbody>
          <tr>
            <td style={{fontWeight:'bolder'}}>ID</td>
            <td style={{fontWeight:'bolder'}}>Destination Start Date</td>
            <td style={{fontWeight:'bolder'}}>Destination End Date</td>
            <td style={{fontWeight:'bolder'}}>Destination</td>
            <td style={{fontWeight:'bolder'}}>User Name</td>
            <td style={{fontWeight:'bolder'}}>Car Name</td>
          </tr>
          {
            booking.map((item, i) =>
              <tr key={i}>
                <td>{item.booking_id}</td>
                <td>{item.start_date}</td>
                <td>{item.end_date}</td>
                <td>{item.destination}</td>
                <td>{item.user.name}</td>
                <td>{item.car.carname}</td>
              </tr>
            )
          }
        </tbody>
      </Table>
     
      
    </div>
  );
}
export default AdminBooking;
